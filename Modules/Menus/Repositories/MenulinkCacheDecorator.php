<?php

namespace TypiCMS\Modules\Menus\Repositories;

use Illuminate\Database\Eloquent\Collection;
use TypiCMS\Modules\Core\Repositories\CacheAbstractDecorator;
use TypiCMS\Modules\Core\Services\Cache\CacheInterface;

class MenulinkCacheDecorator extends CacheAbstractDecorator implements MenulinkInterface
{
    public function __construct(MenulinkInterface $repo, CacheInterface $cache)
    {
        $this->repo = $repo;
        $this->cache = $cache;
    }

    /**
     * Get a menu’s items and children.
     *
     * @param int $id
     * @param bool $all published or all
     * @param int|null $idToExclude exclude record with given ID
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function allFromMenu($id = null, $all = false, $idToExclude = null)
    {
        $cacheKey = md5(config('app.locale').'all'.$all.$id);

        if ($this->cache->has($cacheKey)) {
            return $this->cache->get($cacheKey);
        }

        $models = $this->repo->allFromMenu($id, $all, $idToExclude);

        // Store in cache for next request
        $this->cache->put($cacheKey, $models);

        return $models;
    }
}
